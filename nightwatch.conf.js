const geckodriver = require('geckodriver');
const percy = require('@percy/nightwatch');

module.exports = {
  src_folders: ['tests'],
  output_folder: false,
  custom_commands_path: [percy.path],

  webdriver: {
    start_process: false,
    //server_path: geckodriver.path
    host: 'hub-cloud.browserstack.com',
    port: 443,
  },

  test_settings: {
    default: {
      desiredCapabilities: {
        'bstack:options' : {
          userName: '<username>',
          accessKey: '<access_key>',
          buildName: 'BS-Percy',
          sessionName: 'Demo Test'
        },
        browserName : 'Chrome',
        browserVersion : 'latest',
      }
    }
  }
};
